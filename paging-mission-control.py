import sys
from datetime import datetime, timedelta
import json

"""
Project Name: Paging Mission Control
Author: Samuel "Kody" Doherty
Date: 4/1/2024

Usage: 
`python paging-mission-control.py data.txt`
"""

class TelemetryData:
    def __init__(self, row):
        clean = row.strip().split('|')
        self.timestamp = datetime.strptime(clean[0], '%Y%m%d %H:%M:%S.%f' )
        self.raw_timestamp = clean[0]
        self.satellite_id = clean[1]
        self.red_high_limit = clean[2]
        self.yellow_high_limit = clean[3]
        self.yellow_low_limit = clean[4]
        self.red_low_limit = clean[5]
        self.raw_value = clean[6]
        self.component = clean[7]

def read_file(filename):
    with open(filename, 'r') as f:
        rows = f.readlines()
        data = []
        for row in rows:
            td = TelemetryData(row) 
            data.append(td)
        return data

def create_groups(data):
    groups = {}
    for entry in data:
        
        comp = entry.component
        id = f'{entry.satellite_id}_{comp}'
        if id in groups:
            groups[id].append(entry)
        else: 
            groups[id] = [entry]
    return groups

def build_alert(data, severity, timestamp): 
    formatted_timestamp = timestamp.strftime('%Y-%m-%dT%H:%M:%S.%f')[:-3] + 'Z'
    return json.dumps({
       "satelliteId": data.satellite_id,
       "severity" : severity,
       "component": data.component,
       "timestamp": formatted_timestamp
    })

def low_battery(group):
    clock = 0
    counter = 0 
    alerts = []
    for entry in group:
        if float(entry.raw_value) < float(entry.red_low_limit):
            if clock == 0:
                clock = entry.timestamp
                counter += 1
            else:
                time_diff = entry.timestamp - clock
                if time_diff < timedelta(minutes= 5):
                    counter += 1
                    if counter == 3:
                        alerts.append(build_alert(entry, 'RED LOW', clock))
                else:
                   clock = entry.timestamp 
                   counter = 0         
    return alerts

def high_temp(group):
    clock = 0
    counter = 0 
    alerts = []
    for entry in group:
        if float(entry.raw_value) > float(entry.red_high_limit):
            counter += 1
            if clock == 0:
                clock = entry.timestamp
             
            else:
                time_diff = entry.timestamp - clock
                if time_diff < timedelta(minutes= 5):  
                    if counter == 3:
                        alerts.append(build_alert(entry, 'RED HIGH', clock))
                else:
                   clock = entry.timestamp 
                   counter = 0         
    return alerts

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Please run the script with `python paging-mission-control.py data.txt`")
        sys.exit(1)
    filename = sys.argv[1]
    file_data = read_file(filename)
    groups = create_groups(file_data)
    alerts = []
    for key in groups:
        comp = key.split('_')[1]
        if comp == 'BATT':
            low_bat_results = low_battery(groups[key])
            if len(low_bat_results):
                alerts.extend(low_bat_results)
        else:
            high_temp_results = high_temp(groups[key])
            if len(high_temp_results):
                alerts.extend(high_temp_results)
    print(alerts)
